Welcome to Weather app repo
-------------------------------
Weather app that user open source https://openweathermap.org/ api 
My first MVVM implementation



How to Use:
First pull the repo to local computer
Open terminal and go to project directory
Type 'pod install' to get pods that is use in the project
On project folder open WeatherApp.xcworkspace


Pod used
Alamofire - for network request
https://github.com/Alamofire/Alamofire


Installed but not used, but implementation is ready on custom cell
KingFisher - for image caching
https://github.com/onevcat/Kingfisher


