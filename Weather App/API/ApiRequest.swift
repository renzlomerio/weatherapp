//
//  ApiRequest.swift
//  Weather App
//
//  Created by Lorenz on 7/25/19.
//  Copyright © 2019 sdasd. All rights reserved.
//

import Foundation
import Alamofire

struct ApiRequest {
    
    // function that gets city name as param and completion handler
    static func getWeather(withCityName cityName: String, completion : @escaping (_ response: WeatherRequestResponse?, _ error : Error?) -> ()) {
        
        // build param with cith name and json mode
        let param : Parameters = ["q":cityName,"mode":"JSON"]
        
        // setup openweathermap api key
        let header : HTTPHeaders = ["x-api-key":WeatherAPI.key]
        
        // get method request
        Alamofire.request(WeatherAPI.baseURL, method: .get, parameters: param, encoding: URLEncoding.default, headers: header).responseData { response in
            
            // check error and return completion if true
            if let error = response.error {
                completion(nil, error)
            }else{
                
                // unwrap response data
                if let data = response.result.value {
                    do {
                        // decode response to model
                        let apiResponse = try JSONDecoder().decode(WeatherRequestResponse.self, from: data)
                        completion(apiResponse, nil)
                        print("test")
                    }catch {
                        // return completion with error, failed to decode data
                        
                        // build error desction for alert
                        let errorInfo: [String : Any] = [NSLocalizedDescriptionKey:  NSLocalizedString("Error", value: "Failed to parse data, please check city name", comment: "")]
                        let error = NSError(domain: "", code: 5500, userInfo: errorInfo)
                        
                        // return completion with error
                        completion(nil, error)
                    }
                }
            }
        }
    }
    
}
