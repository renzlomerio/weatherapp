//
//  Constant.swift
//  Weather App
//
//  Created by Lorenz on 7/25/19.
//  Copyright © 2019 sdasd. All rights reserved.
//

import UIKit

struct WeatherAPI {
    static let key = "28243c9ddfad1d521651c8d0c7d41ffe" // key from opnwethermap.org
    static let baseURL = "https://api.openweathermap.org/data/2.5/forecast"
    static let baseImageURL = "http://openweathermap.org/img/wn/"
}

