//
//  WeatherRequestResponse.swift
//  Weather App
//
//  Created by Lorenz on 7/25/19.
//  Copyright © 2019 sdasd. All rights reserved.
//

import UIKit

struct WeatherRequestResponse: Codable {
    
    
    enum CodingKeys : String, CodingKey {
        case cod = "cod"
        case message = "message"
        case cnt = "cnt"
        case list = "list"
        case city = "city"
    }
    
    let cod: String
    let message: Double
    let cnt: Int
    let list: [DetailedWeather]
    let city: City
    
    init(from decoder : Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.cod = try container.decode(String.self, forKey: .cod)
        self.message = try container.decode(Double.self, forKey: .message)
        self.cnt = try container.decode(Int.self, forKey: .cnt)
        self.list = try container.decode([DetailedWeather].self, forKey: .list)
        self.city = try container.decode(City.self, forKey: .city)
    }
    
}


struct City: Codable {
    
    enum CodingKeys : String, CodingKey {
        case id = "id"
        case name = "name"
        case coord = "coord"
        case country = "country"
        case population = "population"
        case timezone = "timezone"
    }
    
    let id: Int
    let name: String
    let coord: Coordinate
    let country: String
    let population: Int
    let timezone: Int
    
    init(from decoder : Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(Int.self, forKey: .id)
        self.name = try container.decode(String.self, forKey: .name)
        self.coord = try container.decode(Coordinate.self, forKey: .coord)
        self.country = try container.decode(String.self, forKey: .country)
        self.population = try container.decode(Int.self, forKey: .population)
        self.timezone = try container.decode(Int.self, forKey: .timezone)
    }
    
}

struct Coordinate: Codable {
    enum CodingKeys : String, CodingKey {
        case lat = "lat"
        case lon = "lon"
    }
    
    let lat: Double
    let lon: Double
    
    init(from decoder : Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.lat = try container.decode(Double.self, forKey: .lat)
        self.lon = try container.decode(Double.self, forKey: .lon)
    }
    
    func encode(to encode : Encoder) throws {
        var container = encode.container(keyedBy: CodingKeys.self)
        try container.encode(self.lat, forKey: .lat)
        try container.encode(self.lon, forKey: .lon)
    }
    
}

struct DetailedWeather: Codable {
    enum CodingKeys : String, CodingKey {
        case weather = "weather"
        case date = "dt_txt"
    }
    
    let weather: [Weather]
    let date: Date
    
    init(from decoder : Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.weather = try container.decode([Weather].self, forKey: .weather)
        let stringDate = try container.decode(String.self, forKey: .date)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        self.date = dateFormatter.date(from: stringDate)!
    }
    
}

struct Weather: Codable {
    
    enum CodingKeys : String, CodingKey {
        case id = "id"
        case main = "main"
        case description = "description"
        case icon = "icon"
    }
    
    let id: Int
    let main: String
    let description: String
    let icon: String
    
    init(from decoder : Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(Int.self, forKey: .id)
        self.main = try container.decode(String.self, forKey: .main)
        self.description = try container.decode(String.self, forKey: .description)
        self.icon = try container.decode(String.self, forKey: .icon)
    }
    
}
