//
//  WeatherCellViewModel.swift
//  Weather App
//
//  Created by Lorenz on 7/26/19.
//  Copyright © 2019 sdasd. All rights reserved.
//

import Foundation


class WeatherCellViewModel {
    
    let id: Int
    let main: String
    let description: String
    let icon: String
    
    init(weather: Weather) {
        self.id = weather.id
        self.main = weather.main
        self.description = weather.description
        self.icon = weather.icon
    }
    
}
