//
//  WeatherViewModel.swift
//  Weather App
//
//  Created by Lorenz on 7/25/19.
//  Copyright © 2019 sdasd. All rights reserved.
//

import UIKit



class WeatherViewModel {
    
    fileprivate var weatherArray = [DetailedWeather]()
    
    var cityNameInput: String = "" {
        didSet {
            searchAction()
        }
    }
    
    var searchCallBack: ((_ error: Error?) -> Void)?
    
    
    // search button action
    func searchAction() {
        
        // check if connected to internet
        if Reachability.isConnectedToNetwork() {
            // call weather request, paasing inputed city name
            ApiRequest.getWeather(withCityName: cityNameInput) { (response, error) in
                
                if let result = response {
                    self.formatWeather(deatailedWeatherArray: result.list)
                }
                
                self.searchCallBack?(error)
            }
        }else{
            // search call back with error, no internet connection
            // build error desction for alert
            let errorInfo: [String : Any] = [NSLocalizedDescriptionKey:  NSLocalizedString("Error", value: "No Internet Connection", comment: "")]
            let error = NSError(domain: "", code: 5500, userInfo: errorInfo)
            
            // return error on call back
            self.searchCallBack?(error)
        }
        
        
    }
    
    fileprivate func formatWeather(deatailedWeatherArray : [DetailedWeather]) {
        // create temp array to hold specific detailed weather
        var array = [DetailedWeather]()
        
        // iterate on detailed weather array
        for dWeather in deatailedWeatherArray {
            
            // validate if temp array does not contain detailed weather with the same date
            if !array.contains(where: { detailedWeather -> Bool in
                return (detailedWeather.date.format(format: "yyyy-MM-dd") == dWeather.date.format(format: "yyyy-MM-dd")) ? true : false
            }) && !dWeather.weather.isEmpty {
                array.append(dWeather)
            }
        }
        
        // pass temp array to model weather array
        weatherArray = array
    }
    
    // return weather section count
    func getWeatherListSectionCount() -> Int {
        return weatherArray.count
    }
    
    // return weather row count
    func getWeatherListRowCount(section: Int) -> Int {
        return weatherArray[section].weather.count
    }
    
    // initialized cell view model
    func getWeatherCellViewModel(indexPath: IndexPath) -> WeatherCellViewModel {
        return WeatherCellViewModel(weather: weatherArray[indexPath.section].weather[indexPath.row])
    }
    
    // format section header title
    func getHeaderTitle(section: Int) -> String {
        return weatherArray[section].date.format(format: "EEEE, MMM dd")
    }
    
    
    
}
