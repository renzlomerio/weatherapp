//
//  ViewController.swift
//  Weather App
//
//  Created by Lorenz on 7/25/19.
//  Copyright © 2019 sdasd. All rights reserved.
//

import UIKit
import Alamofire
import Kingfisher

class ViewController: UIViewController {
    
    var refreshControl = UIRefreshControl()
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
            refreshControl.addTarget(self, action: #selector(searchAction(_:)), for: .valueChanged)
            tableView.addSubview(refreshControl)
        }
    }
    @IBOutlet weak var inputTextField: UITextField!
    
    
    var viewModel = WeatherViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
        // view model search call back action
        viewModel.searchCallBack = { error in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            self.refreshControl.endRefreshing()
            
            guard let err = error else {
                // no error will reload table data
                self.tableView.reloadData()
                return
            }
            
            //show error
            print("error code \(err.localizedDescription)")
            let alert = UIAlertController(title: "Error", message: err.localizedDescription, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "okay", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    

    @IBAction func searchAction(_ sender: Any) {
        guard let input = inputTextField.text else { return }
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        // set input on view model
        viewModel.cityNameInput = input
        
        // dismiss keyboard
        self.view.endEditing(true)
    }
}

extension ViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // get weather list section count from view model
        return viewModel.getWeatherListSectionCount()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // get weather list row count from view model
        return viewModel.getWeatherListRowCount(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // get cell view model
        let weatherCellViewModel = viewModel.getWeatherCellViewModel(indexPath: indexPath)
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "weatherCell", for: indexPath) as! WeatherTableViewCell
        cell.weatherCellViewModel = weatherCellViewModel
          
        return cell
    }
    
}

extension ViewController: UITableViewDelegate {
    
    // set section header height
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    // setup header section view & label
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        // setup section view
        let sectionView = UIView()
        sectionView.frame = CGRect(x: 0, y: 0, width: tableView.frame.width, height: 30)
        sectionView.backgroundColor = .darkGray
        
        //setup section title label
        let titleLabel = UILabel()
        titleLabel.frame = CGRect(x: 20, y: 0, width: UIScreen.main.bounds.size.width - 40, height: 30)
        titleLabel.font = .systemFont(ofSize: 16, weight: .semibold)
        titleLabel.textColor = UIColor.white
        titleLabel.text = viewModel.getHeaderTitle(section: section)
        sectionView.addSubview(titleLabel)
        
        return sectionView
    }
}

