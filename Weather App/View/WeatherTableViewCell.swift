//
//  WeatherTableViewCell.swift
//  Weather App
//
//  Created by Lorenz on 7/25/19.
//  Copyright © 2019 sdasd. All rights reserved.
//

import UIKit
import Kingfisher

class WeatherTableViewCell: UITableViewCell {
    
    @IBOutlet weak var weatherLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    
    var weatherCellViewModel : WeatherCellViewModel! {
        didSet {
            
            // set cell view model data
            weatherLabel.text = weatherCellViewModel.main
            descriptionLabel.text = weatherCellViewModel.description
            
            
            // build image url
            if let imageUrl = URL(string: WeatherAPI.baseImageURL + weatherCellViewModel.icon + ".png") {
                // request image data from uurl
                URLSession.shared.dataTask( with: imageUrl, completionHandler: {
                    (data, response, error) -> Void in
                    DispatchQueue.main.async {
                        if let data = data {
                            // convert data to image andd assign the iconImageView
                            self.iconImageView.image = UIImage(data: data)
                        }
                    }
                }).resume()
            }
            
            // king fisher implementatio
//            cell.iconImageView.kf.setImage(with: imageUrl)
//            cell.iconImageView.kf.indicatorType = .activity

        }
    }
    
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        // set iconImageView.image to nil eveytime the cell is reuse
        iconImageView.image = nil
    }

}
