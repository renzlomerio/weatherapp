//
//  Date+Fomatter.swift
//  Weather App
//
//  Created by Lorenz on 7/26/19.
//  Copyright © 2019 sdasd. All rights reserved.
//

import UIKit

extension Date {
    func format(format : String) -> String {
        let df = DateFormatter()
        df.dateFormat = format
        return df.string(from: self)
    }
}
